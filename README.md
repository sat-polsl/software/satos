[![Pipeline](https://gitlab.com/sat-polsl/software/satos/badges/main/pipeline.svg)](https://gitlab.com/sat-polsl/software/satos/-/pipelines?page=1&scope=all&ref=main)
[![Version](https://img.shields.io/badge/dynamic/json?color=blue&label=Version&query=%24%5B%3A1%5D.name&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F16727548%2Frepository%2Ftags)](https://gitlab.com/sat-polsl/software/satos/-/tags)

# SATOS

C++ wrapper for [FreeRTOS](https://www.freertos.org/).

[Project Wiki](https://sat-polsl.gitlab.io/wiki/satos/) includes setup guide and API documentation.
