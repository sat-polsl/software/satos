#include <cerrno>
#include <sys/stat.h>
#include <sys/times.h>

#if SATOS_DYNAMIC_ALLOCATION
#include <cstdint>
#include <sys/reent.h>
#include "FreeRTOS.h"
#include "task.h"
extern std::uint8_t _heap, _eheap;

extern "C" void __malloc_lock(struct _reent*) { vTaskSuspendAll(); }
extern "C" void __malloc_unlock(struct _reent*) { xTaskResumeAll(); }
extern "C" void vApplicationMallocFailedHook();

extern "C" void* _sbrk(int incr) {
    static std::uint8_t* heap_end = &_heap;
    std::uint8_t* prev_heap_end = heap_end;

    if (heap_end + incr > &_eheap) {
        _impure_ptr->_errno = ENOMEM;
        vApplicationMallocFailedHook();
        return reinterpret_cast<void*>(-1);
    }

    heap_end += incr;
    return reinterpret_cast<void*>(prev_heap_end);
}

#else
extern "C" void* _sbrk(int) {
    errno = ENOMEM;
    return reinterpret_cast<void*>(-1);
}
#endif

extern "C" int _close(int) { return -1; }

extern "C" int _fstat(int, struct stat* st) {
    st->st_mode = S_IFCHR;
    return 0;
}

extern "C" int _isatty(int) { return 1; }

extern "C" int _lseek(int, int, int) { return 0; }

extern "C" [[noreturn]] void _exit(int) {
    for (;;)
        ;
}

extern "C" void _kill(int, int) {
    errno = EINVAL;
    return;
}

extern "C" int _getpid() { return -1; }

extern "C" int _write(int, char*, int) { return -1; }

extern "C" int _read(int, char*, int) { return -1; }

extern "C" int _open(const char*, int, int) { return -1; }

extern "C" int _stat(char*, struct stat* st) {
    st->st_mode = S_IFCHR;
    return 0;
}

extern "C" int _link(char*, char*) {
    errno = EMLINK;
    return -1;
}

extern "C" int _unlink(char*) {
    errno = ENOENT;
    return -1;
}

extern "C" int _wait(int*) {
    errno = ECHILD;
    return -1;
}

extern "C" int _execve(char*, char**, char**) {
    errno = ENOMEM;
    return -1;
}

extern "C" int _fork() {
    errno = EAGAIN;
    return -1;
}

extern "C" int _times(struct tms*) { return -1; }
