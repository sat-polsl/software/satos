list(APPEND CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/cmake)
set(CMAKE_SYSTEM_NAME embedded)
set(CMAKE_CROSSCOMPILING 1)
set(CMAKE_SYSTEM_PROCESSOR ARM)
set(CMAKE_PROCESSOR_TYPE arm)
set(CMAKE_TRY_COMPILE_TARGET_TYPE STATIC_LIBRARY)

set(ARCH_FLAGS -mthumb -mcpu=${TARGET_CORE} -mfloat-abi=${FLOAT_ABI} ${FPU})

# Project wide compiler options
add_compile_options(
    ${ARCH_FLAGS} # Architecture specific flags
    -fno-common # Prevent globals from being put in common ram
    -ffunction-sections # Generate separate ELF section for each function
    -fdata-sections # Enable elf section per variable
    -MD # Create dependencies file

    $<$<COMPILE_LANGUAGE:CXX>:-fno-rtti>
    $<$<COMPILE_LANGUAGE:CXX>:-fno-exceptions>

    $<$<CONFIG:Release>:-O3>
    $<$<CONFIG:Debug>:-O0>

    -Wall
    -Werror
    -Wno-psabi

    $<$<BOOL:${ENABLE_COVERAGE}>:--coverage>
)

# Project wide linker options
add_link_options(
    ${ARCH_FLAGS} # Specify linker path
    -static # Use only static libraries
    -nostartfiles # Don't use standard library for startup
    LINKER:--gc-sections # Delete unused code
    $<$<BOOL:${ENABLE_COVERAGE}>:--coverage>
)

add_compile_definitions(
    $<$<CONFIG:Debug>:DEBUG>
)

set(CMAKE_CXX_LINK_EXECUTABLE "<CMAKE_CXX_COMPILER> <FLAGS> <CMAKE_CXX_LINK_FLAGS> <LINK_FLAGS> -o <TARGET> -Wl,--start-group <LINK_LIBRARIES> <OBJECTS> -Wl,--end-group")
