CPMAddPackage("gl:sat-polsl/software/packages/arm-none-eabi-gcc@13.2.1")

if (SATOS_TEST)
    CPMAddPackage(
        NAME semihosting
        GITLAB_REPOSITORY sat-polsl/software/semihosting
        VERSION 2.0.0
        OPTIONS
        "SEMIHOSTING_COVERAGE_FILTER satos"
    )

    CPMAddPackage("gh:fmtlib/fmt#10.1.1")
endif ()

CPMAddPackage("gl:sat-polsl/software/satext@2.4.1")

CPMAddPackage(
    NAME googletest
    GITHUB_REPOSITORY google/googletest
    GIT_TAG v1.13.0
    OPTIONS
    "gtest_disable_pthreads ON"
    EXCLUDE_FROM_ALL YES
)

set_target_properties(gtest gmock gmock_main
    PROPERTIES
    CXX_STANDARD 20
    CXX_STANDARD_REQUIRED ON
    CXX_EXTENSIONS ON
    )

target_compile_definitions(gtest
    PUBLIC
    _POSIX_PATH_MAX=128
    GTEST_HAS_POSIX_RE=0
    )

target_compile_definitions(gmock
    PUBLIC
    GTEST_HAS_POSIX_RE=0
)
