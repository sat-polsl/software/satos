#pragma once
#include "fmt/format.h"
#include "satos/condition_variable.h"
#include "satos/kernel.h"
#include "satos/thread.h"

template<>
struct fmt::formatter<satos::kernel::state> {
    constexpr auto parse(fmt::format_parse_context& ctx) -> decltype(ctx.begin()) {
        return ctx.end();
    }

    template<typename FormatContext>
    auto format(const satos::kernel::state& s, FormatContext& ctx) -> decltype(ctx.out()) {
        std::string_view name = "unknown";
        switch (s) {
        case satos::kernel::state::error:
            name = "error";
            break;
        case satos::kernel::state::inactive:
            name = "inactive";
            break;
        case satos::kernel::state::ready:
            name = "ready";
            break;
        case satos::kernel::state::running:
            name = "running";
            break;
        case satos::kernel::state::locked:
            name = "locked";
            break;
        case satos::kernel::state::suspended:
            name = "suspended";
            break;
        }
        return fmt::format_to(ctx.out(), "kernel::state::{}", name);
    }
};

template<>
struct fmt::formatter<satos::kernel::error> {
    constexpr auto parse(fmt::format_parse_context& ctx) -> decltype(ctx.begin()) {
        return ctx.end();
    }

    template<typename FormatContext>
    auto format(const satos::kernel::error& e, FormatContext& ctx) -> decltype(ctx.out()) {
        std::string_view name = "unknown";
        switch (e) {
        case satos::kernel::error::nullptr_dereference:
            name = "nullptr_dereference";
            break;
        case satos::kernel::error::out_of_memory:
            name = "out_of_memory";
            break;
        case satos::kernel::error::out_of_range:
            name = "out_of_range";
            break;
        case satos::kernel::error::rtos_assert:
            name = "rtos_assert";
            break;
        }
        return fmt::format_to(ctx.out(), "kernel::error::{}", name);
    }
};

template<>
struct fmt::formatter<satos::thread_state> {
    constexpr auto parse(fmt::format_parse_context& ctx) -> decltype(ctx.begin()) {
        return ctx.end();
    }

    template<typename FormatContext>
    auto format(const satos::thread_state& s, FormatContext& ctx) -> decltype(ctx.out()) {
        std::string_view name = "unknown";
        switch (s) {
        case satos::thread_state::running:
            name = "running";
            break;
        case satos::thread_state::ready:
            name = "ready";
            break;
        case satos::thread_state::inactive:
            name = "inactive";
            break;
        case satos::thread_state::error:
            name = "error";
            break;
        case satos::thread_state::suspended:
            name = "suspended";
            break;
        case satos::thread_state::terminated:
            name = "terminated";
            break;
        case satos::thread_state::blocked:
            name = "blocked";
            break;
        }
        return fmt::format_to(ctx.out(), "thread_state::{}", name);
    }
};

template<>
struct fmt::formatter<satos::thread_priority> {
    constexpr auto parse(fmt::format_parse_context& ctx) -> decltype(ctx.begin()) {
        return ctx.end();
    }

    template<typename FormatContext>
    auto format(const satos::thread_priority& p, FormatContext& ctx) -> decltype(ctx.out()) {
        return fmt::format_to(
            ctx.out(), "{}", static_cast<std::underlying_type_t<satos::thread_priority>>(p));
    }
};

template<>
struct fmt::formatter<satos::cv_status> {
    constexpr auto parse(fmt::format_parse_context& ctx) -> decltype(ctx.begin()) {
        return ctx.end();
    }

    template<typename FormatContext>
    auto format(const satos::cv_status& s, FormatContext& ctx) -> decltype(ctx.out()) {
        std::string_view name = "unknown";
        switch (s) {
        case satos::cv_status::timeout:
            name = "timeout";
            break;
        case satos::cv_status::no_timeout:
            name = "no_timeout";
            break;
        }
        return fmt::format_to(ctx.out(), "cv_status::{}", name);
    }
};

template<>
struct fmt::formatter<satos::clock::time_point> {
    constexpr auto parse(fmt::format_parse_context& ctx) -> decltype(ctx.begin()) {
        return ctx.end();
    }

    template<typename FormatContext>
    auto format(const satos::clock::time_point& t, FormatContext& ctx) -> decltype(ctx.out()) {
        return fmt::format_to(ctx.out(), "{}ms", t.time_since_epoch().count());
    }
};

template<>
struct fmt::formatter<satos::clock::duration> {
    constexpr auto parse(fmt::format_parse_context& ctx) -> decltype(ctx.begin()) {
        return ctx.end();
    }

    template<typename FormatContext>
    auto format(const satos::clock::duration& d, FormatContext& ctx) -> decltype(ctx.out()) {
        return fmt::format_to(ctx.out(), "{}ms", d.count());
    }
};
