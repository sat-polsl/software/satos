set(TARGET satos_formatters)

add_library(${TARGET} INTERFACE)
add_library(satos::formatters ALIAS ${TARGET})

target_include_directories(${TARGET} INTERFACE include)

target_link_libraries(${TARGET}
    INTERFACE
    fmt
    satos
    )