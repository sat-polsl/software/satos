#include <vector>
#include "satext/units.h"
#include "satos/condition_variable.h"
#include "satos/formatters/formatters.h"
#include "satos/kernel.h"
#include "satos/mutex.h"
#include "satos/tester.h"
#include "satos/thread.h"

#if SATOS_DYNAMIC_ALLOCATION
using cv_type = satos::detail::condition_variable;
#else
using cv_type = satos::detail::condition_variable_static<3>;
#endif

using namespace satos;
using namespace satos_tester;
using namespace satos::chrono_literals;
using time_point = satos::clock::time_point;

template<typename T>
class high_prio_thread;
template<typename T>
using high_prio_thread_base = thread<high_prio_thread<T>, thread_priority::normal_0, 8192>;
template<typename T>
class high_prio_thread : public high_prio_thread_base<T> {
    friend high_prio_thread_base<T>;

public:
    high_prio_thread(mutex& mtx, T& cv, bool& flag, std::uint32_t& counter) :
        mtx_(mtx),
        cv_(cv),
        flag_(flag),
        counter_(counter) {}

private:
    template<class clock, class duration>
    void wait_for_next_test(const std::chrono::time_point<clock, duration>& sleep_time) {
        this_thread::sleep_until(sleep_time);
        assert_that(clock::now(), is_eq(sleep_time));
    }

    void thread_function() {
        { // 1. test wait and notify_one
            std::unique_lock lck(mtx_);
            cv_.wait(lck);
            assert_that(clock::now(), is_eq(time_point(20_ms + counter_ * 20_ms)));
            counter_++;
        }

        wait_for_next_test(time_point(100_ms));

        { // 2. test wait and notify_all
            std::unique_lock lck(mtx_);
            cv_.wait(lck);
            assert_that(clock::now(), is_eq(time_point(150_ms)));
        }

        wait_for_next_test(time_point(200_ms));

        { // 3. test wait with predicate and notify_one
            std::unique_lock lck(mtx_);
            cv_.wait(lck, [this]() { return flag_; });
            assert_that(clock::now(), is_eq(time_point(250_ms + counter_ * 20_ms)));
            counter_++;
        }

        wait_for_next_test(time_point(300_ms));

        { // 4. test wait with predicate and notify_all
            std::unique_lock lck(mtx_);
            cv_.wait(lck, [this]() { return flag_; });
            assert_that(clock::now(), is_eq(time_point(350_ms)));
        }

        wait_for_next_test(time_point(400_ms));

        { // 5. test wait_for timeout
            std::unique_lock lck(mtx_);
            auto result = cv_.wait_for(lck, 50_ms);
            assert_that(result, is_eq(cv_status::timeout));
            assert_that(clock::now(), is_eq(time_point(450_ms)));
        }

        wait_for_next_test(time_point(500_ms));

        { // 6. test wait_for no timeout
            std::unique_lock lck(mtx_);
            auto result = cv_.wait_for(lck, 100_ms);
            assert_that(result, is_eq(cv_status::no_timeout));
            assert_that(clock::now(), is_eq(time_point(520_ms + counter_ * 20_ms)));
            counter_++;
        }

        wait_for_next_test(time_point(600_ms));

        { // 7. test wait_for 2x no timeout, last with timeout
            std::unique_lock lck(mtx_);
            auto result = cv_.wait_for(lck, 50_ms);
            if (counter_ != 2) {
                assert_that(result, is_eq(cv_status::no_timeout));
                assert_that(clock::now(), is_eq(time_point(620_ms + counter_ * 20_ms)));
            } else {
                assert_that(result, is_eq(cv_status::timeout));
                assert_that(clock::now(), is_eq(time_point(650_ms)));
            }
            counter_++;
        }

        wait_for_next_test(time_point(700_ms));

        { // 8. test wait_for no timeout notify_all
            std::unique_lock lck(mtx_);
            auto result = cv_.wait_for(lck, 100_ms);
            assert_that(result, is_eq(cv_status::no_timeout));
            assert_that(clock::now(), is_eq(time_point(750_ms)));
        }

        wait_for_next_test(time_point(800_ms));

        { // 9. test wait_for with predicate timeout
            std::unique_lock lck(mtx_);
            auto result = cv_.wait_for(lck, 50_ms, [this]() { return flag_; });
            assert_that(result, is_eq(false));
            assert_that(clock::now(), is_eq(time_point(850_ms)));
        }

        wait_for_next_test(time_point(900_ms));

        { // 10. test wait_for with predicate no timeout
            std::unique_lock lck(mtx_);
            auto result = cv_.wait_for(lck, 100_ms, [this]() { return flag_; });
            assert_that(result, is_eq(true));
            assert_that(clock::now(), is_eq(time_point(950_ms + counter_ * 20_ms)));
            counter_++;
        }

        wait_for_next_test(time_point(1000_ms));

        { // 11. test wait_for with predicate timeout returning true
            std::unique_lock lck(mtx_);
            auto result = cv_.wait_for(lck, 50_ms, [this]() { return flag_; });
            assert_that(result, is_eq(true));
            assert_that(clock::now(), is_eq(time_point(1050_ms)));
        }

        wait_for_next_test(time_point(1100_ms));

        { // 12. test wait_for with predicate notify_all
            std::unique_lock lck(mtx_);
            auto result = cv_.wait_for(lck, 100_ms, [this]() { return flag_; });
            assert_that(result, is_eq(true));
            assert_that(clock::now(), is_eq(time_point(1150_ms)));
        }

        wait_for_next_test(time_point(1200_ms));

        { // 13. test wait_until timeout
            std::unique_lock lck(mtx_);
            auto result = cv_.wait_until(lck, time_point(1250_ms));
            assert_that(result, is_eq(cv_status::timeout));
            assert_that(clock::now(), is_eq(time_point(1250_ms)));
        }

        wait_for_next_test(time_point(1300_ms));

        { // 14. test wait_until no timeout
            std::unique_lock lck(mtx_);
            auto result = cv_.wait_until(lck, time_point(1400_ms));
            assert_that(result, is_eq(cv_status::no_timeout));
            assert_that(clock::now(), is_eq(time_point(1320_ms + counter_ * 20_ms)));
            counter_++;
        }

        wait_for_next_test(time_point(1400_ms));

        { // 15. test wait_until 2x no timeout, last with timeout
            std::unique_lock lck(mtx_);
            auto result = cv_.wait_until(lck, time_point(1450_ms));
            if (counter_ != 2) {
                assert_that(result, is_eq(cv_status::no_timeout));
                assert_that(clock::now(), is_eq(time_point(1420_ms + counter_ * 20_ms)));
            } else {
                assert_that(result, is_eq(cv_status::timeout));
                assert_that(clock::now(), is_eq(time_point(1450_ms)));
            }
            counter_++;
        }

        wait_for_next_test(time_point(1500_ms));

        { // 16. test wait_until no timeout notify_all
            std::unique_lock lck(mtx_);
            auto result = cv_.wait_until(lck, time_point(1600_ms));
            assert_that(result, is_eq(cv_status::no_timeout));
            assert_that(clock::now(), is_eq(time_point(1550_ms)));
        }

        wait_for_next_test(time_point(1600_ms));

        { // 17. test wait_until with predicate timeout
            std::unique_lock lck(mtx_);
            auto result = cv_.wait_until(lck, time_point(1650_ms), [this]() { return flag_; });
            assert_that(result, is_eq(false));
            assert_that(clock::now(), is_eq(time_point(1650_ms)));
        }

        wait_for_next_test(time_point(1700_ms));

        { // 18. test wait_until with predicate no timeout
            std::unique_lock lck(mtx_);
            auto result = cv_.wait_until(lck, time_point(1800_ms), [this]() { return flag_; });
            assert_that(result, is_eq(true));
            assert_that(clock::now(), is_eq(time_point(1750_ms + counter_ * 20_ms)));
            counter_++;
        }

        wait_for_next_test(time_point(1800_ms));

        { // 19. test wait_until with predicate timeout returning true
            std::unique_lock lck(mtx_);
            auto result = cv_.wait_until(lck, time_point(1850_ms), [this]() { return flag_; });
            assert_that(result, is_eq(true));
            assert_that(clock::now(), is_eq(time_point(1850_ms)));
        }

        wait_for_next_test(time_point(1900_ms));

        { // 20. test wait_for with predicate notify_all
            std::unique_lock lck(mtx_);
            auto result = cv_.wait_until(lck, time_point(2000_ms), [this]() { return flag_; });
            assert_that(result, is_eq(true));
            assert_that(clock::now(), is_eq(time_point(1950_ms)));
        }

        wait_for_next_test(time_point(2000_ms));

        end_test();
    }

    mutex& mtx_;
    T& cv_;
    bool& flag_;
    std::uint32_t& counter_;
};

template<typename T>
class low_prio_thread;
template<typename T>
using low_prio_thread_base = thread<low_prio_thread<T>, thread_priority::high_0, 8192>;
template<typename T>
class low_prio_thread : public low_prio_thread_base<T> {
    friend low_prio_thread_base<T>;

public:
    low_prio_thread(mutex& mtx, T& cv, bool& flag, std::uint32_t& counter) :
        mtx_(mtx),
        cv_(cv),
        flag_(flag),
        counter_(counter) {}

private:
    template<class clock, class duration>
    void reset(const std::chrono::time_point<clock, duration>& sleep_time) {
        this_thread::sleep_until(sleep_time);
        assert_that(clock::now(), is_eq(sleep_time));
        counter_ = 0;
        flag_ = false;
    }

    void thread_function() {
        { // 1. test wait and notify_one
            this_thread::sleep_for(20_ms);
            cv_.notify_one();
            this_thread::sleep_for(20_ms);
            cv_.notify_one();
            this_thread::sleep_for(20_ms);
            cv_.notify_one();
        }

        reset(time_point(100_ms));

        { // 2. test wait and notify_all
            this_thread::sleep_for(50_ms);
            cv_.notify_all();
        }

        reset(time_point(200_ms));

        { // 3. test wait with predicate and notify_one
            this_thread::sleep_for(10_ms);
            cv_.notify_one();
            this_thread::sleep_for(10_ms);
            cv_.notify_one();
            this_thread::sleep_for(10_ms);
            cv_.notify_one();

            this_thread::sleep_for(20_ms);
            flag_ = true;
            cv_.notify_one();
            this_thread::sleep_for(20_ms);
            cv_.notify_one();
            this_thread::sleep_for(20_ms);
            cv_.notify_one();
        }

        reset(time_point(300_ms));

        { // 4. test wait with predicate and notify_all
            this_thread::sleep_for(30_ms);
            cv_.notify_all();

            this_thread::sleep_for(20_ms);
            flag_ = true;
            cv_.notify_all();
        }

        reset(time_point(400_ms));

        // 5. test wait_for timeout

        reset(time_point(500_ms));

        { // 6. test wait_for no timeout
            this_thread::sleep_for(20_ms);
            cv_.notify_one();
            this_thread::sleep_for(20_ms);
            cv_.notify_one();
            this_thread::sleep_for(20_ms);
            cv_.notify_one();
        }

        reset(time_point(600_ms));

        { // 7. test wait_for 2x no timeout, last with timeout
            this_thread::sleep_for(20_ms);
            cv_.notify_one();
            this_thread::sleep_for(20_ms);
            cv_.notify_one();
        }

        reset(time_point(700_ms));

        { // 8. test wait_for no timeout notify_all
            this_thread::sleep_for(50_ms);
            cv_.notify_all();
        }

        reset(time_point(800_ms));

        // 9. test wait_for with predicate timeout

        reset(time_point(900_ms));

        { // 10. test wait_for with predicate no timeout
            this_thread::sleep_for(10_ms);
            cv_.notify_one();
            this_thread::sleep_for(10_ms);
            cv_.notify_one();
            this_thread::sleep_for(10_ms);
            cv_.notify_one();

            this_thread::sleep_for(20_ms);
            flag_ = true;
            cv_.notify_one();
            this_thread::sleep_for(20_ms);
            cv_.notify_one();
            this_thread::sleep_for(20_ms);
            cv_.notify_one();
        }

        reset(time_point(1000_ms));

        { // 11. test wait_for with predicate timeout returning true
            this_thread::sleep_for(20_ms);
            flag_ = true;
        }

        reset(time_point(1100_ms));

        { // 12. test wait_for with predicate notify_all
            this_thread::sleep_for(20_ms);
            cv_.notify_all();
            this_thread::sleep_for(30_ms);
            flag_ = true;
            cv_.notify_all();
        }

        reset(time_point(1200_ms));

        // 13. test wait_until timeout

        reset(time_point(1300_ms));

        { // 14. test wait_until no timeout
            this_thread::sleep_for(20_ms);
            cv_.notify_one();
            this_thread::sleep_for(20_ms);
            cv_.notify_one();
            this_thread::sleep_for(20_ms);
            cv_.notify_one();
        }

        reset(time_point(1400_ms));

        { // 15. test wait_until 2x no timeout, last with timeout
            this_thread::sleep_for(20_ms);
            cv_.notify_one();
            this_thread::sleep_for(20_ms);
            cv_.notify_one();
        }

        reset(time_point(1500_ms));

        { // 16. test wait_until no timeout notify_all
            this_thread::sleep_for(50_ms);
            cv_.notify_all();
        }

        reset(time_point(1600_ms));

        // 17. test wait_until with predicate timeout

        reset(time_point(1700_ms));

        { // 18. test wait_until with predicate no timeout
            this_thread::sleep_for(10_ms);
            cv_.notify_one();
            this_thread::sleep_for(10_ms);
            cv_.notify_one();
            this_thread::sleep_for(10_ms);
            cv_.notify_one();

            this_thread::sleep_for(20_ms);
            flag_ = true;
            cv_.notify_one();
            this_thread::sleep_for(20_ms);
            cv_.notify_one();
            this_thread::sleep_for(20_ms);
            cv_.notify_one();
        }

        reset(time_point(1800_ms));

        { // 19. test wait_for with predicate timeout returning true
            this_thread::sleep_for(20_ms);
            flag_ = true;
        }

        reset(time_point(1900_ms));

        { // 20. test wait_for with predicate notify_all
            this_thread::sleep_for(20_ms);
            cv_.notify_all();
            this_thread::sleep_for(30_ms);
            flag_ = true;
            cv_.notify_all();
        }

        reset(time_point(2000_ms));
    }

    mutex& mtx_;
    T& cv_;
    bool& flag_;
    std::uint32_t& counter_;
};

int main(int argv, char** argc) {
    start_test("condition_variable_multiple_threads_test", argv, argc);

    static mutex mtx;
    static cv_type cv;
    static bool flag{false};
    static std::uint32_t counter;
    static high_prio_thread h1(mtx, cv, flag, counter);
    static high_prio_thread h2(mtx, cv, flag, counter);
    static high_prio_thread h3(mtx, cv, flag, counter);
    static std::vector<high_prio_thread<cv_type>*> highs;
    highs.push_back(&h1);
    highs.push_back(&h2);
    highs.push_back(&h3);

    static low_prio_thread low(mtx, cv, flag, counter);

    kernel::initialize(50_MHz, 100_Hz);

    for (auto&& t : highs) {
        t->start();
    }
    low.start();

    kernel::start();
    return 0;
}
