set(TARGET semaphore_test)

add_executable(${TARGET})

target_sources(${TARGET} PRIVATE
    src/semaphore_test.cpp
    )

target_link_libraries(${TARGET} PRIVATE
    satos
    satos_tester
    satos_formatters
    )

set_property(TARGET ${TARGET}
    PROPERTY
    CROSSCOMPILING_EMULATOR ${QEMU_RUNNER} ${QEMU_EXECUTABLE} ${TARGET_CORE}
    )

gtest_discover_tests(${TARGET})
