set(TARGET timer_periodic_test)

add_executable(${TARGET})

target_sources(${TARGET} PRIVATE
    src/timer_periodic_test.cpp
    )

target_link_libraries(${TARGET} PRIVATE
    satos
    satos_tester
    )

set_property(TARGET ${TARGET}
    PROPERTY
    CROSSCOMPILING_EMULATOR ${QEMU_RUNNER} ${QEMU_EXECUTABLE} ${TARGET_CORE}
    )

gtest_discover_tests(${TARGET})
