#include "satext/units.h"
#include "satos/kernel.h"
#include "satos/tester.h"
#include "satos/thread.h"
#include "satos/timer.h"

using namespace satos;
using namespace satos_tester;
using namespace satos::chrono_literals;

template<typename T>
class test_thread;

template<typename T>
using test_thread_base = thread<test_thread<T>, thread_priority::normal_0, 2048>;

template<typename T>
class test_thread : public test_thread_base<T> {
    friend test_thread_base<T>;

public:
    explicit test_thread(std::uint32_t& counter, T& timer) : counter_(counter), timer_(timer) {}

private:
    void thread_function() {
        { // run timer for 1s with period 100ms, test if counter has proper value
            this_thread::sleep_for(1050_ms);

            assert_that(counter_, is_eq(10));
            assert_that(timer_.is_running(), is_eq(true));
        }

        { // test timer stop
            timer_.stop();
            this_thread::sleep_for(10_ms);
            assert_that(timer_.is_running(), is_eq(false));
        }

        { // test if stopped timer wont call callback
            this_thread::sleep_for(500_ms);

            assert_that(timer_.is_running(), is_eq(false));
            assert_that(counter_, is_eq(10));
        }

        { // test timer start, run timer for 1s with period 100ms, test if counter has proper value
            timer_.start();

            this_thread::sleep_for(1050_ms);

            assert_that(timer_.is_running(), is_eq(true));
            assert_that(counter_, is_eq(20));
        }

        { // test timer start and period change
            timer_.start(20_ms);
            this_thread::sleep_for(500_ms);
            assert_that(counter_, is_eq(44));
        }

        end_test();
    }

    std::uint32_t& counter_;
    T& timer_;
};

int main(int argv, char** argc) {
    start_test("timer_periodic_test", argv, argc);

    static std::uint32_t counter{};
    static timer t(100_ms);
    static test_thread test(counter, t);
    t.register_callback([]() { counter++; });

    kernel::initialize(50_MHz, 100_Hz);

    t.start();
    test.start();

    kernel::start();
    return 0;
}
