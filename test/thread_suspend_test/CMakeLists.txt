set(TARGET thread_suspend_test)

add_executable(${TARGET})

target_sources(${TARGET} PRIVATE
    src/thread_suspend_test.cpp
    )

target_link_libraries(${TARGET} PRIVATE
    satos
    satos_tester
    satos_formatters
    )

set_property(TARGET ${TARGET}
    PROPERTY
    CROSSCOMPILING_EMULATOR ${QEMU_RUNNER} ${QEMU_EXECUTABLE} ${TARGET_CORE}
    )

gtest_discover_tests(${TARGET})
