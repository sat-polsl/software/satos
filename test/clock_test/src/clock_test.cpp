#include "satext/units.h"
#include "satos/kernel.h"
#include "satos/tester.h"
#include "satos/thread.h"
#include "satos/timer.h"

using namespace satos;
using namespace satos_tester;
using namespace satos::chrono_literals;
using time_point = satos::clock::time_point;

class test_thread;

using test_thread_base = thread<test_thread, thread_priority::normal_0, 2048>;

class test_thread : public test_thread_base {
    friend test_thread_base;

public:
    explicit test_thread() = default;

private:
    void thread_function() {
        time_point t;

        t = satos::clock::next_time_point(100_ms);
        assert_eq(t, time_point{100_ms});

        t = satos::clock::next_time_point(1_s);
        assert_eq(t, time_point{1_s});

        t = satos::clock::next_time_point(10_s);
        assert_eq(t, time_point{10_s});

        this_thread::sleep_for(6500_ms);
        t = satos::clock::next_time_point(100_ms);
        assert_eq(t, time_point{6600_ms});

        t = satos::clock::next_time_point(1_s);
        assert_eq(t, time_point{7_s});

        t = satos::clock::next_time_point(10_s);
        assert_eq(t, time_point{10_s});

        end_test();
    }
};

int main(int argv, char** argc) {
    start_test("clock_test", argv, argc);

    static test_thread test;

    kernel::initialize(50_MHz, 100_Hz);

    test.start();

    kernel::start();
    return 0;
}
