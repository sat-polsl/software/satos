set(TARGET kernel_error_handler_test)

add_executable(${TARGET})

target_sources(${TARGET} PRIVATE
    src/kernel_error_handler_test.cpp
    )

target_link_libraries(${TARGET} PRIVATE
    satos
    satos_tester
    satos_formatters
    )

set_property(TARGET ${TARGET}
    PROPERTY
    CROSSCOMPILING_EMULATOR ${QEMU_RUNNER} ${QEMU_EXECUTABLE} ${TARGET_CORE}
    )

gtest_discover_tests(${TARGET})
