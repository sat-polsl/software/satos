#include "satext/units.h"
#include "satos/formatters/formatters.h"
#include "satos/kernel.h"
#include "satos/message_queue.h"
#include "satos/tester.h"
#include "satos/thread.h"

using namespace satos;
using namespace satos_tester;
using namespace satos::chrono_literals;
using time_point = satos::clock::time_point;

class consumer_thread;
using consumer_thread_base = thread<consumer_thread, thread_priority::high_0, 4096>;
class consumer_thread : public consumer_thread_base {
    friend consumer_thread_base;

public:
    consumer_thread(message_queue<int, 8>& queue) : queue_(queue) {}

private:
    void thread_function() {
        { // 1. push and pull w/o timeout
            auto v = queue_.pull(100_ms);
            assert_that(v.has_value(), is_eq(true));
            assert_that(*v, is_eq(42));
            assert_that(clock::now(), is_eq(time_point(50_ms)));
        }

        { // 2. pull with timeout
            auto v = queue_.pull(50_ms);
            assert_that(v.has_value(), is_eq(false));
            assert_that(clock::now(), is_eq(time_point(100_ms)));
        }

        { // 3. push to full queue
            this_thread::sleep_until(time_point(150_ms));
            assert_that(queue_.empty(), is_eq(false));

            for (std::uint32_t i = 0; i < queue_.capacity() + 1; i++) {
                auto b = queue_.pull(10_ms);
                assert_that(b.has_value(), is_eq(true));
                assert_that(*b, is_eq(i));
            }
            assert_that(queue_.empty(), is_eq(true));
        }

        this_thread::sleep_for(100_ms);

        { // 4. clear
            assert_that(queue_.empty(), is_eq(true));
            for (std::uint32_t i = 0; i < queue_.capacity(); i++) {
                auto b = queue_.push(i);
                assert_that(b, is_eq(true));
                assert_that(queue_.size(), is_eq(i + 1));
            }
            queue_.clear();

            assert_that(queue_.empty(), is_eq(true));
        }

        end_test();
    }

    message_queue<int, 8>& queue_;
};

class producer_thread;
using producer_thread_base = thread<producer_thread, thread_priority::normal_0, 4096>;
class producer_thread : public producer_thread_base {
    friend producer_thread_base;

public:
    producer_thread(message_queue<int, 8>& queue) : queue_(queue) {}

private:
    void thread_function() {
        assert_that(queue_.capacity(), is_eq(8));
        assert_that(queue_.size(), is_eq(0));

        { // 1. push and pull w/o timeout
            this_thread::sleep_for(50_ms);
            auto b = queue_.push({42});
            assert_that(b, is_eq(true));
        }

        { // 2. pull with timeout
            this_thread::sleep_for(50_ms);
        }

        { // 3. push to full queue
            for (std::uint32_t i = 0; i < queue_.capacity(); i++) {
                auto b = queue_.push(i);
                assert_that(b, is_eq(true));
                assert_that(queue_.size(), is_eq(i + 1));
            }

            auto b = queue_.push(0);
            assert_that(b, is_eq(false));

            b = queue_.push({8}, 100_ms);
            assert_that(b, is_eq(true));
            assert_that(clock::now(), is_eq(time_point(150_ms)));
        }
    }

    message_queue<int, 8>& queue_;
};

int main(int argv, char** argc) {
    start_test("message_queue_test", argv, argc);

    static message_queue<int, 8> q;
    static consumer_thread c(q);
    static producer_thread p(q);

    kernel::initialize(50_MHz, 100_Hz);

    c.start();
    p.start();

    kernel::start();
    return 0;
}
