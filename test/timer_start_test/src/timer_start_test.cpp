#include "satext/units.h"
#include "satos/formatters/formatters.h"
#include "satos/kernel.h"
#include "satos/tester.h"
#include "satos/timer.h"

using namespace satos;
using namespace satos_tester;
using namespace satos::chrono_literals;
using time_point = satos::clock::time_point;

int main(int argv, char** argc) {
    start_test("timer_start_test", argv, argc);

    static timer t(100_ms);
    t.register_callback([]() {
        { // test if timer is running
            assert_that(t.is_running(), is_eq(true));
        }

        { // test if timer callback was called after timer period
            assert_that(clock::now(), is_eq(time_point(100_ms)));
        }

        end_test();
    });

    kernel::initialize(50_MHz, 100_Hz);
    t.start();

    assert_that(t.is_running(), is_eq(false));

    kernel::start();
    return 0;
}
