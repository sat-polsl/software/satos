#include "satext/units.h"
#include "satos/formatters/formatters.h"
#include "satos/kernel.h"
#include "satos/tester.h"
#include "satos/thread.h"

using namespace satos;
using namespace satos_tester;
using namespace satos::chrono_literals;
using time_point = satos::clock::time_point;

class test_thread;
using test_thread_base = thread<test_thread>;
class test_thread : public test_thread_base {
    friend test_thread_base;

private:
    void thread_function() {
        { // test if thread starts at 0 ms
            assert_that(clock::now(), is_eq(time_point(0_ms)));
        }

        { // test sleep_for
            this_thread::sleep_for(100_ms);
            assert_that(clock::now(), is_eq(time_point(100_ms)));
        }

        { // test sleep_until
            this_thread::sleep_until(clock::now() + 100_ms);
            assert_that(clock::now(), is_eq(time_point(200_ms)));
        }

        end_test();
    }
};

int main(int argv, char** argc) {
    start_test("this_thread_sleep_test", argv, argc);
    static test_thread t;

    kernel::initialize(50_MHz, 100_Hz);
    t.start();
    kernel::start();
    return 0;
}
