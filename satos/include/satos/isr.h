#pragma once

namespace satos {
class isr {
public:
    isr();
    isr(const isr&) = delete;
    isr& operator=(const isr&) = delete;
    isr(isr&&) noexcept = delete;
    isr& operator=(isr&&) noexcept = delete;
    ~isr();
};
} // namespace satos