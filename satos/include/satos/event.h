#pragma once

#include "satos/clock.h"
#include "satos/concepts/event.h"
#include "satos/critical_section.h"
#include "satos/thread.h"

namespace satos {

class event {
public:
    event();
    event(const event&) = delete;
    event& operator=(const event&) = delete;
    event(event&&) noexcept = delete;
    event& operator=(event&&) noexcept = delete;
    ~event() noexcept;

    void notify();

    void wait();

    [[nodiscard]] bool wait_for(clock::duration timeout);

    [[nodiscard]] bool wait_until(clock::time_point sleep_time);

    void attach_thread();

    void detach_thread();

private:
    class impl;
    impl& impl_;

    static constexpr std::size_t size_of_impl = 4;

    alignas(4) std::byte impl_storage_[size_of_impl];
};

static_assert(concepts::event<event>);

template<satos::concepts::event Event>
class event_guard {
public:
    explicit event_guard(Event& event) : event_{event} { event_.attach_thread(); }
    ~event_guard() { event_.detach_thread(); }

    inline void wait() { event_.wait(); };

    [[nodiscard]] inline bool wait_for(clock::duration timeout) { return event_.wait_for(timeout); }

    [[nodiscard]] inline bool wait_until(clock::time_point sleep_time) {
        return event_.wait_until(sleep_time);
    }

private:
    Event& event_;
};

} // namespace satos
