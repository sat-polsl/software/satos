#pragma once

#include "satext/type_traits.h"
#include "satos/clock.h"
#include "satos/concepts/timer.h"
#include "satos/kernel.h"
#include "satos/timer_autoreload.h"
#if SATOS_DYNAMIC_ALLOCATION
#include <list>
#endif

namespace satos {

namespace detail {
class timer_base {
public:
    timer_base(const timer_base&) = delete;
    timer_base& operator=(const timer_base&) = delete;
    timer_base(timer_base&&) noexcept = delete;
    timer_base& operator=(timer_base&&) noexcept = delete;

    void initialize();

    void start();

    void start(clock::duration timer_period);

    void stop();

    [[nodiscard]] bool is_running();

protected:
    using callback_ptr = void (*)(void*);
    timer_base(chrono::milliseconds timer_period,
               autoreload reload,
               callback_ptr callback_function);

    ~timer_base();

    template<typename T>
    static void callback(void* timer_handle) {
        auto timer = static_cast<T*>(timer_base::get_timer_id(timer_handle));

        if (timer == nullptr) {
            kernel::error_handler(kernel::error::nullptr_dereference);
            return;
        }

        for (auto& c : timer->callbacks_) {
            if (c) {
                c();
            }
        }
    }

private:
    class impl;
    impl& impl_;

#ifndef DEBUG
    static constexpr std::size_t size_of_impl = 60;
#else
    static constexpr std::size_t size_of_impl = 64;
#endif
    alignas(4) std::byte impl_storage_[size_of_impl];

    static void* get_timer_id(void* handle);
};

template<size_t max_callbacks = 1>
class timer_static : public timer_base {
    friend timer_base;

public:
    explicit timer_static(clock::duration timer_period = chrono::milliseconds(1),
                          autoreload reload = autoreload::enabled) :
        timer_base(timer_period, reload, &timer_base::callback<timer_static>) {}

    timer_static(const timer_static&) = delete;
    timer_static& operator=(const timer_static&) = delete;
    timer_static(timer_static&&) noexcept = delete;
    timer_static& operator=(timer_static&&) noexcept = delete;
    ~timer_static() noexcept = default;

    void register_callback(satext::inplace_function<void()> callback) {
        if (callbacks_end_ != callbacks_.end()) {
            *callbacks_end_ = std::move(callback);
            callbacks_end_++;
        } else {
            kernel::error_handler(kernel::error::out_of_range);
        }
    }

private:
    std::array<satext::inplace_function<void()>, max_callbacks> callbacks_;
    satext::inplace_function<void()>* callbacks_end_{callbacks_.begin()};
};

#if SATOS_DYNAMIC_ALLOCATION
class timer : public timer_base {
    friend timer_base;

public:
    explicit timer(clock::duration timer_period = chrono::milliseconds(1),
                   autoreload reload = autoreload::enabled) :
        timer_base(timer_period, reload, &timer_base::callback<timer>) {}

    timer(const timer&) = delete;
    timer& operator=(const timer&) = delete;
    timer(timer&&) noexcept = delete;
    timer& operator=(timer&&) noexcept = delete;
    ~timer() noexcept = default;

    void register_callback(satext::inplace_function<void()> callback) {
        callbacks_.emplace_back(std::move(callback));
    }

private:
    std::list<satext::inplace_function<void()>> callbacks_;
};
#endif // SATOS_DYNAMIC_ALLOCATION
} // namespace detail

static_assert(concepts::timer<detail::timer_static<1>>);

#if SATOS_DYNAMIC_ALLOCATION
using timer = detail::timer;
static_assert(concepts::timer<detail::timer>);
template<size_t max_callbacks>
using timer_static = detail::timer_static<max_callbacks>;
#else
template<size_t max_callbacks = 1>
using timer = detail::timer_static<max_callbacks>;
#endif // SATOS_DYNAMIC_ALLOCATION

} // namespace satos
