#pragma once

namespace satos {
class critical_section {
public:
    critical_section();
    critical_section(const critical_section&) = delete;
    critical_section& operator=(const critical_section&) = delete;
    critical_section(critical_section&&) noexcept = delete;
    critical_section& operator=(critical_section&&) noexcept = delete;
    ~critical_section();
};
} // namespace satos
