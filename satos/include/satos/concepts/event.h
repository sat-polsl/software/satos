#pragma once
#include <concepts>
#include "satos/clock.h"

namespace satos::concepts {
// clang-format off
template<typename T>
concept event = requires(T event, clock::duration duration, clock::time_point time_point) {
    { event.notify() } -> std::same_as<void>;
    { event.wait() } -> std::same_as<void>;
    { event.wait_for(duration) } -> std::same_as<bool>;
    { event.wait_until(time_point) } -> std::same_as<bool>;
    { event.attach_thread() } -> std::same_as<void>;
    { event.detach_thread() } -> std::same_as<void>;
};
// clang-format on
}; // namespace satos::concepts
