#pragma once
#include <concepts>
#include "satos/clock.h"

namespace satos::concepts {
// clang-format off
template<typename T>
concept semaphore = requires(T semaphore, clock::duration duration,
                                     clock::time_point time_point) {
    { semaphore.release() } -> std::same_as<void>;
    { semaphore.acquire() } -> std::same_as<void>;
    { semaphore.try_acquire() } -> std::same_as<bool>;
    { semaphore.try_acquire_for(duration) } -> std::same_as<bool>;
    { semaphore.try_acquire_until(time_point) } -> std::same_as<bool>;
};
// clang-format on
}; // namespace satos::concepts
