#pragma once
#include <concepts>
#include <optional>
#include "satos/clock.h"

namespace satos::concepts {
// clang-format off
template<typename T>
concept flags = requires(T flags, clock::duration duration, std::uint32_t bits, bool clear,
                                 clock::time_point time_point) {
    { flags.wait(bits, clear, duration) } -> std::same_as<std::optional<std::uint32_t>>;
    { flags.wait_for_all(bits, clear, duration) } -> std::same_as<std::optional<std::uint32_t>>;
    { flags.wait(bits, clear, time_point) } -> std::same_as<std::optional<std::uint32_t>>;
    { flags.wait_for_all(bits, clear, time_point) } -> std::same_as<std::optional<std::uint32_t>>;
    { flags.set(bits) } -> std::same_as<std::optional<std::uint32_t>>;
    { flags.clear(bits) } -> std::same_as<std::optional<std::uint32_t>>;
    { flags.get() } -> std::same_as<std::optional<std::uint32_t>>;
    { flags.sync(bits, bits, duration) } -> std::same_as<std::optional<std::uint32_t>>;
};
// clang-format on
}; // namespace satos::concepts
