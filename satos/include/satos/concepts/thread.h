#pragma once
#include <concepts>
#include "satos/kernel.h"
#include "satos/thread.h"

namespace satos::concepts {
// clang-format off
template<typename T>
concept thread = requires(T thread) {
    { thread.start() } -> std::same_as<void>;
    { thread.stop() } -> std::same_as<void>;
    { thread.suspend() } -> std::same_as<satos::kernel::status>;
    { thread.resume() } -> std::same_as<satos::kernel::status>;
    { thread.get_state() } -> std::same_as<satos::thread_state>;
    { thread.get_priority() } -> std::same_as<satos::thread_priority>;
    { thread.native_handle() } -> std::same_as<satos::thread_native_handle>;
};
// clang-format on
}; // namespace satos::concepts
