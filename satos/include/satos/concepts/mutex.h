#pragma once
#include <concepts>
#include "satos/clock.h"

namespace satos::concepts {
// clang-format off
template<typename T>
concept mutex = requires(T mutex, clock::duration duration, clock::time_point time_point) {
    { mutex.lock() } -> std::same_as<void>;
    { mutex.unlock() } -> std::same_as<void>;
    { mutex.try_lock() } -> std::same_as<bool>;
    { mutex.try_lock_for(duration) } -> std::same_as<bool>;
    { mutex.try_lock_until(time_point) } -> std::same_as<bool>;
};
// clang-format on
}; // namespace satos::concepts
