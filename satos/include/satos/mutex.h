#pragma once

#include "satos/clock.h"
#include "satos/concepts/mutex.h"
#include "satos/kernel.h"

namespace satos {
using mutex_native_handle = void*;

class mutex {
public:
    mutex();
    mutex(const mutex&) = delete;
    mutex& operator=(const mutex&) = delete;
    mutex(mutex&&) noexcept = delete;
    mutex& operator=(mutex&&) noexcept = delete;
    ~mutex() noexcept;

    void lock();

    void unlock();

    [[nodiscard]] mutex_native_handle native_handle();

    [[nodiscard]] bool try_lock();

    [[nodiscard]] bool try_lock_for(satos::clock::duration timeout);

    [[nodiscard]] bool try_lock_until(satos::clock::time_point sleep_time);

private:
    class impl;
    impl& impl_;

#ifndef DEBUG
    static constexpr std::size_t size_of_impl = 76;
#else
    static constexpr std::size_t size_of_impl = 84;
#endif
    alignas(4) std::byte impl_storage_[size_of_impl];
};

class recursive_mutex {
public:
    recursive_mutex();
    recursive_mutex(const recursive_mutex&) = delete;
    recursive_mutex& operator=(const recursive_mutex&) = delete;
    recursive_mutex(recursive_mutex&&) noexcept = delete;
    recursive_mutex& operator=(recursive_mutex&&) noexcept = delete;
    ~recursive_mutex() noexcept;

    void lock();

    void unlock();

    [[nodiscard]] mutex_native_handle native_handle();

    [[nodiscard]] bool try_lock();

    [[nodiscard]] bool try_lock_for(satos::clock::duration timeout);

    [[nodiscard]] bool try_lock_until(satos::clock::time_point sleep_time);

private:
    class impl;
    impl& impl_;

#ifndef DEBUG
    static constexpr std::size_t size_of_impl = 76;
#else
    static constexpr std::size_t size_of_impl = 84;
#endif
    alignas(4) std::byte impl_storage_[size_of_impl];
};

static_assert(concepts::mutex<mutex>);
static_assert(concepts::mutex<recursive_mutex>);

} // namespace satos
