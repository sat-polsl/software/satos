#pragma once
#include <chrono>
#include <cstdint>

namespace satos {
namespace chrono {
using nanoseconds = std::chrono::duration<std::uint32_t, std::nano>;
using microseconds = std::chrono::duration<std::uint32_t, std::micro>;
using milliseconds = std::chrono::duration<std::uint32_t, std::milli>;
using seconds = std::chrono::duration<std::uint32_t>;
using minutes = std::chrono::duration<std::uint32_t, std::ratio<60>>;
using hours = std::chrono::duration<std::uint32_t, std::ratio<3600>>;
} // namespace chrono

namespace chrono_literals {
consteval auto operator"" _ns(unsigned long long int value) {
    return satos::chrono::nanoseconds(value);
}

consteval auto operator"" _us(unsigned long long int value) {
    return satos::chrono::microseconds(value);
}

consteval auto operator"" _ms(unsigned long long int value) {
    return satos::chrono::milliseconds(value);
}

consteval auto operator"" _s(unsigned long long int value) { return satos::chrono::seconds(value); }

consteval auto operator"" _min(unsigned long long int value) {
    return satos::chrono::minutes(value);
}

consteval auto operator"" _h(unsigned long long int value) { return satos::chrono::hours(value); }
} // namespace chrono_literals
} // namespace satos
