#pragma once
#include <optional>
#include "satos/clock.h"
#include "satos/concepts/flags.h"
#include "satos/kernel.h"

namespace satos {
class flags {
public:
    flags();
    flags(const flags&) = delete;
    flags& operator=(const flags&) = delete;
    flags(flags&&) noexcept = delete;
    flags& operator=(flags&&) = delete;
    ~flags();

    std::optional<std::uint32_t> wait(std::uint32_t bits_to_wait,
                                      bool clear_on_exit,
                                      clock::duration timeout = clock::duration::max());

    std::optional<std::uint32_t>
    wait(std::uint32_t bits_to_wait, bool clear_on_exit, clock::time_point timepoint);

    std::optional<std::uint32_t> wait_for_all(std::uint32_t bits_to_wait,
                                              bool clear_on_exit,
                                              clock::duration timeout = clock::duration::max());

    std::optional<std::uint32_t>
    wait_for_all(std::uint32_t bits_to_wait, bool clear_on_exit, clock::time_point timepoint);

    std::optional<std::uint32_t> set(std::uint32_t bits_to_set);

    std::optional<std::uint32_t> clear(std::uint32_t bits_to_set);

    std::optional<std::uint32_t> get();

    std::optional<std::uint32_t> sync(std::uint32_t bits_to_set,
                                      std::uint32_t bits_to_wait,
                                      clock::duration timeout = clock::duration::max());

private:
    class impl;
    impl& impl_;

#ifndef DEBUG
    static constexpr std::size_t size_of_impl = 28;
#else
    static constexpr std::size_t size_of_impl = 32;
#endif

    alignas(4) std::byte impl_storage_[size_of_impl];
};

static_assert(concepts::flags<flags>);

} // namespace satos
