#pragma once

#include <mutex>
#include "satos/clock.h"
#include "satos/kernel.h"
#include "satos/message_queue.h"
#include "satos/mutex.h"
#include "satos/thread.h"
#if SATOS_DYNAMIC_ALLOCATION
#include <list>
#endif

namespace satos {

#if SATOS_DYNAMIC_ALLOCATION
template<class T,
         thread_priority priority = thread_priority::normal_0,
         size_t stack_size = 1024,
         size_t queue_size = 16>
class actor : private thread<actor<T, priority, stack_size, queue_size>, priority, stack_size> {
    using thread_base = thread<actor<T, priority, stack_size, queue_size>, priority, stack_size>;
    friend thread_base;

    static_assert(stack_size != 0, "Stack size cannot be zero");

private:
    template<typename message_type>
    using signal = satext::inplace_function<void(message_type&)>;

    struct actor_parcel {
        virtual ~actor_parcel() = default;
        virtual void deliver_to(T* derived) = 0;
        virtual bool is_dynamically_allocated() { return true; }
    };

    template<typename message_type>
    struct actor_signal : public actor_parcel {
        signal<message_type> message;
        explicit actor_signal(signal<message_type>&& msg) : message(msg) {}
        void deliver_to(T*) override { signals<T, message_type>().push_back(message); }
    };

public:
    actor() = default;
    actor(const actor&) = delete;
    actor& operator=(const actor&) = delete;
    actor(actor&&) noexcept = delete;
    actor& operator=(actor&&) noexcept = delete;
    ~actor() noexcept = default;

    template<typename message_type>
    class actor_message : public actor_parcel {
        friend actor;

    private:
        message_type message{};
        inline static std::list<actor_message<message_type>*> dynamically_allocated;

        bool is_dynamically_allocated() override {
            return std::find(dynamically_allocated.begin(), dynamically_allocated.end(), this) !=
                   dynamically_allocated.end();
        }

        static void* operator new(size_t size) {
            void* ret = ::operator new(size);
            dynamically_allocated.push_back(static_cast<actor_message<message_type>*>(ret));
            return ret;
        }

        static void operator delete(void* ptr) {
            auto casted_ptr = static_cast<actor_message<message_type>*>(ptr);
            if (auto it = std::find(
                    dynamically_allocated.begin(), dynamically_allocated.end(), casted_ptr);
                it != dynamically_allocated.end()) {
                dynamically_allocated.erase(it);
                ::operator delete(ptr);
            }
        }

        void deliver_to(T* derived) override { derived->on_message(message); }

    public:
        actor_message() = default;
        explicit actor_message(message_type&& msg) : message(std::move(msg)) {}
    };

    void start() {
        thread_base::start();
        dispatching_ = true;
    }

    template<typename message_type>
    void send(message_type msg) {
        post<actor_message<message_type>>(std::move(msg));
    }

    template<typename message_type>
    void send(actor_message<message_type>* msg) {
        post<actor_message<message_type>>(msg);
    }

    template<typename message_type>
    signal<message_type> get_slot() const {
        auto this_ptr = const_cast<actor<T, priority, stack_size, queue_size>*>(this);
        return signal<message_type>(
            [this_ptr](message_type& data) { static_cast<T*>(this_ptr)->send(std::move(data)); });
    }

    template<typename message_type>
    void connect(signal<message_type> slot) {
        post<actor_signal<message_type>>(std::move(slot));
    }

    template<typename message_type, typename him>
    void connect(const him* receiver) {
        connect(receiver->template get_slot<message_type>());
    }

    void stop() {
        std::unique_lock lck(mtx_);
        if (dispatching_) {
            dispatching_ = false;
            static_cast<void>(queue_.push(nullptr));
        }
    }

protected:
    template<typename message_type>
    void emit(message_type msg) {
        for (auto& c : signals<T, message_type>()) {
            if (c)
                c(msg);
        }
    }

private:
    void on_start() {}
    void on_stop() {}

    void thread_function() {
        auto derived = static_cast<T*>(this);

        if (derived == nullptr) {
            kernel::error_handler(kernel::error::nullptr_dereference);
            return;
        }

        derived->on_start();
        while (true) {
            derived->event_loop();
            std::unique_lock lck(derived->mtx_);
            if (!derived->dispatching_)
                break;
        }
        derived->on_stop();
    }

    void event_loop() {
        auto derived = static_cast<T*>(this);

        if (derived == nullptr) {
            kernel::error_handler(kernel::error::nullptr_dereference);
            return;
        }

        while (true) {
            {
                std::unique_lock lck(mtx_);
                if (!dispatching_)
                    break;
            }
            auto msg = queue_.pull(clock::duration::max());
            if (msg.has_value() && msg.value() != nullptr) {
                msg.value()->deliver_to(derived);
                if (msg.value()->is_dynamically_allocated()) {
                    delete msg.value();
                }
            }
        }
    }

    template<typename parcelable, typename message_type>
    void post(message_type&& msg) {
        if (!is_irq()) {
            {
                std::unique_lock lck(mtx_);
                if (!dispatching_)
                    return;
            }
            static_cast<void>(queue_.push(new parcelable(std::move(msg))));
        }
    }

    template<typename parcelable>
    void post(parcelable* msg) {
        if (is_irq() && dispatching_) {
            static_cast<void>(queue_.push(msg));
        }
    }

    template<typename thread, typename message_type>
    inline static std::list<signal<message_type>>& signals() {
        static std::list<signal<message_type>> callbacks;
        return callbacks;
    }

    message_queue<actor_parcel*, queue_size> queue_{};
    mutex mtx_{};
    bool dispatching_{false};
};
#endif // SATOS_DYNAMIC_ALLOCATION
} // namespace satos
