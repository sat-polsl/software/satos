#include "satos/thread.h"
#include "FreeRTOS.h"
#include "task.h"

namespace satos::detail {
class thread_base::impl {
public:
    impl() = default;
    impl(const impl&) = delete;
    impl& operator=(const impl&) = delete;
    impl(impl&&) noexcept = delete;
    impl& operator=(impl&&) noexcept = delete;
    ~impl() noexcept;

    void start(thread_base* thread_base,
               thread_base::thread_funtion_ptr thread_function,
               std::uint32_t* stack,
               size_t stack_size,
               thread_priority priority,
               const char* name);

    void stop();

    kernel::status suspend();

    kernel::status resume();

    [[nodiscard]] thread_state get_state() const;

    [[nodiscard]] thread_priority get_priority() const;

    [[nodiscard]] thread_native_handle native_handle();

private:
    StaticTask_t thread_control_block_{};
    TaskHandle_t thread_handle_{nullptr};
};

void thread_base::impl::start(thread_base* thread_base,
                              thread_base::thread_funtion_ptr thread_function,
                              std::uint32_t* stack,
                              size_t stack_size,
                              thread_priority priority,
                              const char* name) {
    if (!is_isr()) {
        if ((priority < thread_priority::idle) || (priority > thread_priority::isr)) {
            return;
        }
        thread_handle_ = xTaskCreateStatic(thread_function,
                                           name,
                                           stack_size,
                                           thread_base,
                                           static_cast<UBaseType_t>(priority),
                                           stack,
                                           &thread_control_block_);
    }
}

kernel::status thread_base::impl::suspend() {
    if (is_isr()) {
        return kernel::status::error_isr;
    } else if (thread_handle_ == nullptr) {
        return kernel::status::error;
    } else {
        vTaskSuspend(thread_handle_);
        return kernel::status::ok;
    }
}

kernel::status thread_base::impl::resume() {
    if (is_isr()) {
        return kernel::status::error_isr;
    } else if (thread_handle_ == nullptr) {
        return kernel::status::error;
    } else {
        vTaskResume(thread_handle_);
        return kernel::status::ok;
    }
}

thread_state thread_base::impl::get_state() const {
    if (is_isr() || thread_handle_ == nullptr) {
        return thread_state::error;
    }

    auto kernel_ready = kernel::get_state() == kernel::state::ready;

    switch (eTaskGetState(thread_handle_)) {
    case eRunning:
        return kernel_ready ? thread_state::inactive : thread_state::running;
    case eReady:
        return kernel_ready ? thread_state::inactive : thread_state::ready;
    case eBlocked:
        return thread_state::blocked;
    case eSuspended:
        return thread_state::suspended;
    case eDeleted:
        return thread_state::terminated;
    case eInvalid:
    default:
        return thread_state::error;
    }
}

thread_priority thread_base::impl::get_priority() const {
    if (is_isr() || thread_handle_ == nullptr) {
        return thread_priority::error;
    } else {
        return static_cast<thread_priority>(uxTaskPriorityGet(thread_handle_));
    }
}

thread_native_handle thread_base::impl::native_handle() { return thread_handle_; }

void thread_base::impl::stop() { vTaskDelete(thread_handle_); }

thread_base::impl::~impl() noexcept {
    if (get_state() != thread_state::terminated) {
        vTaskDelete(thread_handle_);
    }
}

thread_base::thread_base() : impl_{*reinterpret_cast<impl*>(&impl_storage_)}, impl_storage_{} {
    static_assert(sizeof(impl) == sizeof(impl_storage_));
    static_assert(std::alignment_of<impl>::value == alignof(impl_storage_));
    new (&impl_storage_) impl;
}

thread_base::~thread_base() noexcept { reinterpret_cast<impl*>(&impl_storage_)->~impl(); }

void thread_base::stop() { impl_.stop(); }

void thread_base::start(thread_base::thread_funtion_ptr thread_function,
                        std::uint32_t* stack,
                        size_t stack_size,
                        thread_priority priority,
                        const char* name) {
    impl_.start(this, thread_function, stack, stack_size, priority, name);
}

kernel::status thread_base::suspend() { return impl_.suspend(); }

kernel::status thread_base::resume() { return impl_.resume(); }

thread_state thread_base::get_state() const { return impl_.get_state(); }

thread_priority thread_base::get_priority() const { return impl_.get_priority(); }

thread_native_handle thread_base::native_handle() { return impl_.native_handle(); }
} // namespace satos::detail

namespace satos::this_thread {
void yield() {
    if (!detail::is_isr()) {
        taskYIELD();
    }
}

kernel::status suspend() {
    if (detail::is_isr()) {
        return kernel::status::error_isr;
    }
    auto thread_handle = xTaskGetCurrentTaskHandle();
    if (thread_handle == nullptr) {
        return kernel::status::error;
    } else {
        vTaskSuspend(thread_handle);
        return kernel::status::ok;
    }
}

kernel::status resume() {
    if (detail::is_isr()) {
        return kernel::status::error_isr;
    }
    auto thread_handle = xTaskGetCurrentTaskHandle();
    if (thread_handle == nullptr) {
        return kernel::status::error;
    } else {
        vTaskResume(thread_handle);
        return kernel::status::ok;
    }
}

void stop() { vTaskDelete(nullptr); }

thread_native_handle get_native_handle() { return xTaskGetCurrentTaskHandle(); }

void sleep_for(satos::clock::duration sleep_duration) {
    auto ticks = satos::clock::to_ticks(sleep_duration);
    if (!detail::is_isr()) {
        if (ticks != 0) {
            vTaskDelay(ticks);
        }
    }
}

void sleep_until(satos::clock::time_point sleep_time) {
    auto ticks = satos::clock::to_ticks(sleep_time);
    if (!detail::is_isr()) {
        TickType_t now = xTaskGetTickCount();
        vTaskDelayUntil(&now, (ticks - now));
    }
}

} // namespace satos::this_thread
