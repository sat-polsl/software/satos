#include "satos/event.h"
#include "FreeRTOS.h"
#include "task.h"

namespace satos {
class event::impl {
public:
    impl() = default;
    impl(const impl&) = delete;
    impl& operator=(const impl&) = delete;
    impl(impl&&) noexcept = delete;
    impl& operator=(impl&&) noexcept = delete;
    ~impl() noexcept = default;

    void notify();
    void wait();
    [[nodiscard]] bool wait_for(clock::duration timeout);
    [[nodiscard]] bool wait_until(clock::time_point sleep_time);
    void attach_thread();
    void detach();

private:
    thread_native_handle thread_{nullptr};
};

void event::impl::notify() {
    auto thread = reinterpret_cast<TaskHandle_t>(thread_);
    if (thread == nullptr) {
        return;
    }
    if (detail::is_isr()) {
        xTaskNotifyFromISR(thread, 0, eNoAction, kernel::get_isr_yield());
    } else {
        xTaskNotify(thread, 0, eNoAction);
    }
}

void event::impl::wait() { xTaskNotifyWait(0, 0, nullptr, portMAX_DELAY); }

bool event::impl::wait_for(clock::duration timeout) {
    return xTaskNotifyWait(0, 0, nullptr, satos::clock::to_ticks(timeout));
}

bool event::impl::wait_until(clock::time_point sleep_time) {
    auto now = clock::now();
    return xTaskNotifyWait(0, 0, nullptr, satos::clock::to_ticks(sleep_time - now));
}

void event::impl::attach_thread() {
    critical_section c;
    kernel::expects(thread_ == nullptr);
    thread_ = this_thread::get_native_handle();
}

void event::impl::detach() { thread_ = nullptr; }

event::event() : impl_{*reinterpret_cast<impl*>(&impl_storage_)}, impl_storage_{} {
    static_assert(sizeof(impl) == sizeof(impl_storage_));
    static_assert(std::alignment_of<impl>::value == alignof(impl_storage_));
    new (&impl_storage_) impl;
}

event::~event() noexcept { reinterpret_cast<impl*>(&impl_storage_)->~impl(); }

void event::wait() { impl_.wait(); }

void event::notify() { impl_.notify(); }

bool event::wait_for(clock::duration timeout) { return impl_.wait_for(timeout); }

bool event::wait_until(clock::time_point sleep_time) { return impl_.wait_until(sleep_time); }

void event::attach_thread() { impl_.attach_thread(); }

void event::detach_thread() { impl_.detach(); }

} // namespace satos
