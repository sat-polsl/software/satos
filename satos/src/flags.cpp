#include "satos/flags.h"
#include "FreeRTOS.h"
#include "event_groups.h"

namespace satos {
class flags::impl {
public:
    impl();
    impl(const impl&) = delete;
    impl& operator=(const impl&) = delete;
    impl(impl&&) noexcept = delete;
    impl& operator=(impl&&) noexcept = delete;
    ~impl() noexcept;

    std::optional<std::uint32_t>
    wait(std::uint32_t bits_to_wait, bool clear_on_exit, std::uint32_t ticks);

    std::optional<std::uint32_t>
    wait_for_all(std::uint32_t bits_to_wait, bool clear_on_exit, std::uint32_t ticks);

    std::optional<std::uint32_t> set(std::uint32_t bits_to_set);

    std::optional<std::uint32_t> clear(std::uint32_t bits_to_set);

    std::optional<std::uint32_t> get();

    std::optional<std::uint32_t>
    sync(std::uint32_t bits_to_set, std::uint32_t bits_to_wait, std::uint32_t ticks);

private:
    EventGroupHandle_t event_group_handle_{nullptr};
    StaticEventGroup_t event_group_block_;
};

flags::impl::impl() {
    if (!detail::is_isr()) {
        event_group_handle_ = xEventGroupCreateStatic(&event_group_block_);
    }
};

flags::impl::~impl() {
    if (!detail::is_isr()) {
        vEventGroupDelete(event_group_handle_);
    }
}
std::optional<std::uint32_t>
flags::impl::wait(std::uint32_t bits_to_wait, bool clear_on_exit, std::uint32_t ticks) {
    if (event_group_handle_ == nullptr) {
        kernel::error_handler(kernel::error::nullptr_dereference);
        return std::nullopt;
    }
    if (detail::is_isr()) {
        return std::nullopt;
    }

    std::uint32_t result =
        xEventGroupWaitBits(event_group_handle_, bits_to_wait, clear_on_exit, pdFALSE, ticks);
    if ((result & bits_to_wait) != 0) {
        return result;
    } else {
        return std::nullopt;
    }
}

std::optional<std::uint32_t>
flags::impl::wait_for_all(std::uint32_t bits_to_wait, bool clear_on_exit, std::uint32_t ticks) {
    if (event_group_handle_ == nullptr) {
        kernel::error_handler(kernel::error::nullptr_dereference);
        return std::nullopt;
    }
    if (detail::is_isr()) {
        return std::nullopt;
    }

    std::uint32_t result =
        xEventGroupWaitBits(event_group_handle_, bits_to_wait, clear_on_exit, pdTRUE, ticks);
    if ((result & bits_to_wait) == bits_to_wait) {
        return result;
    } else {
        return std::nullopt;
    }
}

std::optional<std::uint32_t> flags::impl::set(std::uint32_t bits_to_set) {
    if (event_group_handle_ == nullptr) {
        kernel::error_handler(kernel::error::nullptr_dereference);
        return std::nullopt;
    }
    if (detail::is_isr()) {
        return xEventGroupSetBitsFromISR(event_group_handle_, bits_to_set, kernel::get_isr_yield());
    } else {
        return xEventGroupSetBits(event_group_handle_, bits_to_set);
    }
}

std::optional<std::uint32_t> flags::impl::clear(std::uint32_t bits_to_clear) {
    if (event_group_handle_ == nullptr) {
        kernel::error_handler(kernel::error::nullptr_dereference);
        return std::nullopt;
    }
    if (detail::is_isr()) {
        return xEventGroupClearBitsFromISR(event_group_handle_, bits_to_clear);
    } else {
        return xEventGroupClearBits(event_group_handle_, bits_to_clear);
    }
}

std::optional<std::uint32_t> flags::impl::get() {
    if (event_group_handle_ == nullptr) {
        kernel::error_handler(kernel::error::nullptr_dereference);
        return std::nullopt;
    }
    if (detail::is_isr()) {
        return xEventGroupGetBitsFromISR(event_group_handle_);
    } else {
        return xEventGroupGetBits(event_group_handle_);
    }
}
std::optional<std::uint32_t>
flags::impl::sync(std::uint32_t bits_to_set, std::uint32_t bits_to_wait, std::uint32_t ticks) {
    if (event_group_handle_ == nullptr) {
        kernel::error_handler(kernel::error::nullptr_dereference);
        return std::nullopt;
    }
    if (detail::is_isr()) {
        return std::nullopt;
    }

    std::uint32_t result = xEventGroupSync(event_group_handle_, bits_to_set, bits_to_wait, ticks);
    if ((result & bits_to_wait) == bits_to_wait) {
        return result;
    } else {
        return std::nullopt;
    }
}

flags::flags() : impl_{*reinterpret_cast<impl*>(&impl_storage_)}, impl_storage_{} {
    static_assert(sizeof(impl) == sizeof(impl_storage_));
    static_assert(std::alignment_of<impl>::value == alignof(impl_storage_));
    new (&impl_storage_) impl;
}

flags::~flags() noexcept { reinterpret_cast<impl*>(&impl_storage_)->~impl(); }

std::optional<std::uint32_t>
flags::wait(std::uint32_t bits_to_wait, bool clear_on_exit, clock::duration timeout) {
    return impl_.wait(bits_to_wait, clear_on_exit, satos::clock::to_ticks(timeout));
}
std::optional<std::uint32_t>
flags::wait_for_all(std::uint32_t bits_to_wait, bool clear_on_exit, clock::duration timeout) {
    return impl_.wait_for_all(bits_to_wait, clear_on_exit, satos::clock::to_ticks(timeout));
}
std::optional<std::uint32_t> flags::set(std::uint32_t bits_to_set) {
    return impl_.set(bits_to_set);
}
std::optional<std::uint32_t> flags::clear(std::uint32_t bits_to_set) {
    return impl_.clear(bits_to_set);
}
std::optional<std::uint32_t> flags::get() { return impl_.get(); }

std::optional<std::uint32_t>
flags::sync(std::uint32_t bits_to_set, std::uint32_t bits_to_wait, clock::duration timeout) {
    return impl_.sync(bits_to_set, bits_to_wait, satos::clock::to_ticks(timeout));
}

std::optional<std::uint32_t>
flags::wait(std::uint32_t bits_to_wait, bool clear_on_exit, clock::time_point timepoint) {
    auto ticks = satos::clock::to_ticks(timepoint);
    auto now = xTaskGetTickCount();
    return impl_.wait(bits_to_wait, clear_on_exit, ticks - now);
}

std::optional<std::uint32_t>
flags::wait_for_all(std::uint32_t bits_to_wait, bool clear_on_exit, clock::time_point timepoint) {
    auto ticks = satos::clock::to_ticks(timepoint);
    auto now = xTaskGetTickCount();
    return impl_.wait_for_all(bits_to_wait, clear_on_exit, ticks - now);
}

} // namespace satos
