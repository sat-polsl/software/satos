#pragma once
#include "gmock/gmock.h"
#include "satos/clock.h"
#include "satos/kernel.h"
#include "satos/thread.h"

namespace satos::mock {
class this_thread {
public:
    this_thread(const this_thread&) = delete;
    this_thread& operator=(const this_thread&) = delete;
    this_thread(this_thread&&) noexcept = delete;
    this_thread& operator=(this_thread&&) noexcept = delete;
    ~this_thread() noexcept = default;

    MOCK_METHOD(void, yield, ());
    MOCK_METHOD(kernel::status, suspend, ());
    MOCK_METHOD(kernel::status, resume, ());
    MOCK_METHOD(void, stop, ());
    MOCK_METHOD(void, sleep_for, (satos::clock::duration));
    MOCK_METHOD(void, sleep_until, (satos::clock::time_point));
    MOCK_METHOD(thread_native_handle, get_native_handle, ());

    static this_thread& instance();

    static void reset();

private:
    this_thread() = default;
};
} // namespace satos::mock
