#pragma once
#include "gmock/gmock.h"
#include "satos/concepts/thread.h"
#include "satos/kernel.h"
#include "satos/thread.h"

namespace satos::mock {
struct thread {
    MOCK_METHOD(void, start, ());
    MOCK_METHOD(void, stop, ());
    MOCK_METHOD(satos::kernel::status, suspend, ());
    MOCK_METHOD(satos::kernel::status, resume, ());
    MOCK_METHOD(satos::thread_state, get_state, ());
    MOCK_METHOD(satos::thread_priority, get_priority, ());
    MOCK_METHOD(satos::thread_native_handle, native_handle, ());
};

static_assert(satos::concepts::thread<satos::mock::thread>);

} // namespace satos::mock
