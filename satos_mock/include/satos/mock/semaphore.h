#pragma once

#include <vector>
#include "gmock/gmock.h"
#include "satos/concepts/semaphore.h"
#include "satos/kernel.h"

namespace satos::mock {
struct semaphore {
    semaphore() { semaphores.push_back(this); }
    ~semaphore() {
        semaphores.erase(std::remove(semaphores.begin(), semaphores.end(), this), semaphores.end());
    }

    MOCK_METHOD(void, release, ());
    MOCK_METHOD(void, acquire, ());
    MOCK_METHOD(bool, try_acquire, ());
    MOCK_METHOD(bool, try_acquire_for, (const satos::kernel::clock::duration&));
    MOCK_METHOD(bool, try_acquire_until, (const satos::kernel::clock::time_point&));

    inline static std::vector<semaphore*> semaphores{};
};

static_assert(satos::concepts::semaphore<satos::mock::semaphore>);
} // namespace satos::mock
