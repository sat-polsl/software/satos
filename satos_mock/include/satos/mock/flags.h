#pragma once

#include <optional>
#include <vector>
#include "gmock/gmock.h"
#include "satos/concepts/flags.h"
#include "satos/kernel.h"

namespace satos::mock {
struct flags {
    flags() { flags_vector.push_back(this); }
    ~flags() {
        flags_vector.erase(std::remove(flags_vector.begin(), flags_vector.end(), this),
                           flags_vector.end());
    }

    MOCK_METHOD(std::optional<std::uint32_t>, wait, (std::uint32_t, bool, satos::clock::duration));
    MOCK_METHOD(std::optional<std::uint32_t>,
                wait_for_all,
                (std::uint32_t, bool, satos::clock::duration));
    MOCK_METHOD(std::optional<std::uint32_t>,
                wait,
                (std::uint32_t, bool, satos::clock::time_point));
    MOCK_METHOD(std::optional<std::uint32_t>,
                wait_for_all,
                (std::uint32_t, bool, satos::clock::time_point));
    MOCK_METHOD(std::optional<std::uint32_t>, set, (std::uint32_t));
    MOCK_METHOD(std::optional<std::uint32_t>, clear, (std::uint32_t));
    MOCK_METHOD(std::optional<std::uint32_t>, get, ());
    MOCK_METHOD(std::optional<std::uint32_t>,
                sync,
                (std::uint32_t, std::uint32_t, satos::clock::duration));

    inline static std::vector<flags*> flags_vector{};
};

static_assert(satos::concepts::flags<satos::mock::flags>);
} // namespace satos::mock
