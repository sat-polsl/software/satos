#pragma once

#include <vector>
#include "gmock/gmock.h"
#include "satos/clock.h"
#include "satos/concepts/mutex.h"

namespace satos::mock {
struct mutex {
    mutex() { mutexes.push_back(this); }
    ~mutex() { mutexes.erase(std::remove(mutexes.begin(), mutexes.end(), this), mutexes.end()); }

    MOCK_METHOD(void, lock, ());
    MOCK_METHOD(void, unlock, ());
    MOCK_METHOD(bool, try_lock, ());
    MOCK_METHOD(bool, try_lock_for, (const satos::clock::duration&));
    MOCK_METHOD(bool, try_lock_until, (const satos::clock::time_point&));

    inline static std::vector<mutex*> mutexes{};
};

static_assert(satos::concepts::mutex<satos::mock::mutex>);
} // namespace satos::mock
