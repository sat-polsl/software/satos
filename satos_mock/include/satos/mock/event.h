#pragma once

#include <vector>
#include "gmock/gmock.h"
#include "satos/clock.h"
#include "satos/concepts/event.h"

namespace satos::mock {
struct event {
    event() { events.push_back(this); }
    ~event() { events.erase(std::remove(events.begin(), events.end(), this), events.end()); }

    MOCK_METHOD(void, notify, ());
    MOCK_METHOD(void, wait, ());
    MOCK_METHOD(bool, wait_for, (const satos::clock::duration&));
    MOCK_METHOD(bool, wait_until, (const satos::clock::time_point&));
    MOCK_METHOD(void, attach_thread, ());
    MOCK_METHOD(void, detach_thread, ());

    inline static std::vector<event*> events{};
};

static_assert(satos::concepts::event<satos::mock::event>);
} // namespace satos::mock
