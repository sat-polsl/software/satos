#include "satos/flags.h"
#include "satos/mock/flags.h"

namespace satos {
class flags::impl {
public:
    impl() = default;
    impl(const impl&) = delete;
    impl& operator=(const impl&) = delete;
    impl(impl&&) noexcept = delete;
    impl& operator=(impl&&) noexcept = delete;
    ~impl() noexcept = default;

    std::optional<std::uint32_t>
    wait(std::uint32_t bits_to_wait, bool clear_on_exit, satos::clock::duration duration);

    std::optional<std::uint32_t>
    wait_for_all(std::uint32_t bits_to_wait, bool clear_on_exit, satos::clock::duration duration);

    std::optional<std::uint32_t>
    wait(std::uint32_t bits_to_wait, bool clear_on_exit, satos::clock::time_point timepoint);

    std::optional<std::uint32_t> wait_for_all(std::uint32_t bits_to_wait,
                                              bool clear_on_exit,
                                              satos::clock::time_point timepoint);

    std::optional<std::uint32_t> set(std::uint32_t bits_to_set);

    std::optional<std::uint32_t> clear(std::uint32_t bits_to_set);

    std::optional<std::uint32_t> get();

    std::optional<std::uint32_t>
    sync(std::uint32_t bits_to_set, std::uint32_t bits_to_wait, satos::clock::duration duration);

private:
    mock::flags flags_;
};

std::optional<std::uint32_t>
flags::impl::wait(std::uint32_t bits_to_wait, bool clear_on_exit, satos::clock::duration duration) {
    return flags_.wait(bits_to_wait, clear_on_exit, duration);
}

std::optional<std::uint32_t> flags::impl::wait_for_all(std::uint32_t bits_to_wait,
                                                       bool clear_on_exit,
                                                       satos::clock::duration duration) {
    return flags_.wait_for_all(bits_to_wait, clear_on_exit, duration);
}

std::optional<std::uint32_t> flags::impl::wait(std::uint32_t bits_to_wait,
                                               bool clear_on_exit,
                                               satos::clock::time_point timepoint) {
    return flags_.wait(bits_to_wait, clear_on_exit, timepoint);
}

std::optional<std::uint32_t> flags::impl::wait_for_all(std::uint32_t bits_to_wait,
                                                       bool clear_on_exit,
                                                       satos::clock::time_point timepoint) {
    return flags_.wait_for_all(bits_to_wait, clear_on_exit, timepoint);
}

std::optional<std::uint32_t> flags::impl::set(std::uint32_t bits_to_set) {
    return flags_.set(bits_to_set);
}

std::optional<std::uint32_t> flags::impl::clear(std::uint32_t bits_to_set) {
    return flags_.clear(bits_to_set);
}

std::optional<std::uint32_t> flags::impl::get() { return flags_.get(); }

std::optional<std::uint32_t> flags::impl::sync(std::uint32_t bits_to_set,
                                               std::uint32_t bits_to_wait,
                                               satos::clock::duration duration) {
    return flags_.sync(bits_to_set, bits_to_wait, duration);
}

flags::flags() : impl_{*new impl}, impl_storage_{} {}

flags::~flags() noexcept { delete &impl_; }

std::optional<std::uint32_t>
flags::wait(std::uint32_t bits_to_wait, bool clear_on_exit, clock::duration timeout) {
    return impl_.wait(bits_to_wait, clear_on_exit, timeout);
}

std::optional<std::uint32_t>
flags::wait_for_all(std::uint32_t bits_to_wait, bool clear_on_exit, clock::duration timeout) {
    return impl_.wait_for_all(bits_to_wait, clear_on_exit, timeout);
}

std::optional<std::uint32_t> flags::set(std::uint32_t bits_to_set) {
    return impl_.set(bits_to_set);
}

std::optional<std::uint32_t> flags::clear(std::uint32_t bits_to_set) {
    return impl_.clear(bits_to_set);
}

std::optional<std::uint32_t> flags::get() { return impl_.get(); }

std::optional<std::uint32_t>
flags::sync(std::uint32_t bits_to_set, std::uint32_t bits_to_wait, clock::duration timeout) {
    return impl_.sync(bits_to_set, bits_to_wait, timeout);
}

std::optional<std::uint32_t>
flags::wait(std::uint32_t bits_to_wait, bool clear_on_exit, clock::time_point timepoint) {
    return impl_.wait(bits_to_wait, clear_on_exit, timepoint);
}

std::optional<std::uint32_t>
flags::wait_for_all(std::uint32_t bits_to_wait, bool clear_on_exit, clock::time_point timepoint) {
    return impl_.wait_for_all(bits_to_wait, clear_on_exit, timepoint);
}
} // namespace satos
