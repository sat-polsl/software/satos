#include "satos/event.h"
#include "satos/mock/event.h"

namespace satos {
class event::impl {
public:
    impl() = default;
    impl(const impl&) = delete;
    impl& operator=(const impl&) = delete;
    impl(impl&&) noexcept = delete;
    impl& operator=(impl&&) noexcept = delete;
    ~impl() noexcept = default;

    void notify();
    void wait();
    [[nodiscard]] bool wait_for(clock::duration timeout);
    [[nodiscard]] bool wait_until(clock::time_point sleep_time);
    void attach_thread();
    void detach_thread();

private:
    mock::event event_;
};

void event::impl::notify() { event_.notify(); }

void event::impl::wait() { event_.wait(); }

bool event::impl::wait_for(clock::duration timeout) { return event_.wait_for(timeout); }

bool event::impl::wait_until(clock::time_point sleep_time) { return event_.wait_until(sleep_time); }

void event::impl::attach_thread() { event_.attach_thread(); }

void event::impl::detach_thread() { event_.detach_thread(); }

event::event() : impl_{*new impl}, impl_storage_{} {}

event::~event() noexcept { delete &impl_; }

void event::wait() { impl_.wait(); }

void event::notify() { impl_.notify(); }

bool event::wait_for(clock::duration timeout) { return impl_.wait_for(timeout); }

bool event::wait_until(clock::time_point sleep_time) { return impl_.wait_until(sleep_time); }

void event::attach_thread() { impl_.attach_thread(); }

void event::detach_thread() { impl_.detach_thread(); }

} // namespace satos
