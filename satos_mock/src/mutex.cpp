#include "satos/mutex.h"
#include "satos/mock/mutex.h"

namespace satos {
class mutex::impl {
public:
    impl() = default;
    impl(const impl&) = delete;
    impl& operator=(const impl&) = delete;
    impl(impl&&) noexcept = delete;
    impl& operator=(impl&&) noexcept = delete;
    ~impl() noexcept = default;

    void lock();
    bool try_lock();
    bool try_lock_for(satos::clock::duration timeout);
    bool try_lock_until(satos::clock::time_point sleep_time);
    void unlock();

private:
    mock::mutex mtx_;
};

void mutex::impl::lock() { mtx_.lock(); }

bool mutex::impl::try_lock() { return mtx_.try_lock(); }

bool mutex::impl::try_lock_for(satos::clock::duration timeout) {
    return mtx_.try_lock_for(timeout);
}

bool mutex::impl::try_lock_until(satos::clock::time_point sleep_time) {
    return mtx_.try_lock_until(sleep_time);
}

void mutex::impl::unlock() { mtx_.unlock(); }

void mutex::lock() { impl_.lock(); }

bool mutex::try_lock() { return impl_.try_lock(); }

bool mutex::try_lock_for(satos::clock::duration timeout) { return impl_.try_lock_for(timeout); }

bool mutex::try_lock_until(satos::clock::time_point sleep_time) {
    return impl_.try_lock_until(sleep_time);
}

void mutex::unlock() { impl_.unlock(); }

mutex_native_handle mutex::native_handle() { return nullptr; }

mutex::mutex() : impl_{*new impl}, impl_storage_{} {}

mutex::~mutex() noexcept { delete &impl_; }

class recursive_mutex::impl {
public:
    impl() = default;
    impl(const impl&) = delete;
    impl& operator=(const impl&) = delete;
    impl(impl&&) noexcept = delete;
    impl& operator=(impl&&) noexcept = delete;
    ~impl() noexcept = default;

    void lock();
    bool try_lock();
    bool try_lock_for(satos::clock::duration timeout);
    bool try_lock_until(satos::clock::time_point sleep_time);
    void unlock();

private:
    mock::mutex mtx_;
};

void recursive_mutex::impl::lock() { mtx_.lock(); }

bool recursive_mutex::impl::try_lock() { return mtx_.try_lock(); }

bool recursive_mutex::impl::try_lock_for(satos::clock::duration timeout) {
    return mtx_.try_lock_for(timeout);
}

bool recursive_mutex::impl::try_lock_until(satos::clock::time_point sleep_time) {
    return mtx_.try_lock_until(sleep_time);
}

void recursive_mutex::impl::unlock() { mtx_.unlock(); }

recursive_mutex::recursive_mutex() : impl_{*new impl}, impl_storage_{} {}

void recursive_mutex::lock() { impl_.lock(); }

void recursive_mutex::unlock() { impl_.unlock(); }

mutex_native_handle recursive_mutex::native_handle() { return nullptr; }

recursive_mutex::~recursive_mutex() noexcept { delete &impl_; }

bool recursive_mutex::try_lock() { return impl_.try_lock(); }

bool recursive_mutex::try_lock_for(satos::clock::duration timeout) {
    return impl_.try_lock_for(timeout);
}

bool recursive_mutex::try_lock_until(satos::clock::time_point sleep_time) {
    return impl_.try_lock_until(sleep_time);
}

} // namespace satos
