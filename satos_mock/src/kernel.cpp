#include "satos/mock/kernel.h"

namespace satos {
namespace mock {
kernel& kernel::instance() {
    static kernel c;
    return c;
}
void kernel::reset() { ::testing::Mock::VerifyAndClear(&instance()); }
} // namespace mock

kernel::status kernel::initialize(std::uint32_t cpu_hz, std::uint32_t tick_rate_hz) {
    return mock::kernel::instance().initialize(cpu_hz, tick_rate_hz);
}

kernel::status kernel::start() { return mock::kernel::instance().start(); }

void kernel::stop() { mock::kernel::instance().stop(); }

kernel::state kernel::get_state() { return mock::kernel::instance().get_state(); }

kernel::status kernel::lock() { return mock::kernel::instance().lock(); }

kernel::status kernel::unlock() { return mock::kernel::instance().unlock(); }

std::uint32_t kernel::get_tick_count() { return mock::kernel::instance().get_tick_count(); }

std::uint32_t kernel::get_tick_freq() { return mock::kernel::instance().get_tick_freq(); }

std::uint32_t kernel::get_tick_duration_ms() {
    return mock::kernel::instance().get_tick_duration_ms();
}

void kernel::enter_critical() { mock::kernel::instance().enter_critical(); }

void kernel::exit_critical() { mock::kernel::instance().exit_critical(); }

void kernel::idle_handler() { mock::kernel::instance().idle_handler(); }

void kernel::error_handler(kernel::error error) { mock::kernel::instance().error_handler(error); }

void kernel::set_idle_handler(const satext::inplace_function<void()> handler) {
    mock::kernel::instance().set_idle_handler(handler);
}

void kernel::set_error_handler(const satext::inplace_function<void(kernel::error)> handler) {
    mock::kernel::instance().set_error_handler(handler);
}

void kernel::isr_push() { mock::kernel::instance().isr_push(); }

std::int32_t kernel::isr_pop() { return mock::kernel::instance().isr_pop(); }

std::int32_t* kernel::get_isr_yield() { return mock::kernel::instance().get_isr_yield(); }

void kernel::expects(bool x) { mock::kernel::instance().expects(x); }

} // namespace satos
