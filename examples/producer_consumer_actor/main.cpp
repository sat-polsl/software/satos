#include "satext/units.h"
#include "satos/actor.h"
#include "satos/kernel.h"
#include "satos/timer.h"

struct product {
    std::int32_t value;
};
struct produce {};

using namespace std::chrono_literals;

class consumer_actor;
using consumer_actor_base = satos::actor<consumer_actor>;

class consumer_actor : public consumer_actor_base {
    friend consumer_actor_base;

private:
    void on_message(product& p) { /*  do_stuff(p.val) */
    }
};

class producer_actor;
using producer_actor_base = satos::actor<producer_actor>;

class producer_actor : public producer_actor_base {
    friend producer_actor_base;

public:
    explicit producer_actor(consumer_actor& cons) : consumer(cons) {}

private:
    void on_message(produce&) {
        static std::int32_t x{};
        consumer.send(product{x++});
    }

    consumer_actor& consumer;
};

satos::timer timer(100ms);

int main() {
    static consumer_actor consumer;
    static producer_actor producer(consumer);

    timer.register_callback([]() { producer.send(produce{}); });

    satos::kernel::initialize(100_MHz, 100_Hz);
    producer.run();
    consumer.run();
    satos::kernel::start();
    while (true) {}
}
