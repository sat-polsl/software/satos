#include "satext/units.h"
#include "satos/condition_variable.h"
#include "satos/kernel.h"
#include "satos/thread.h"

using namespace std::chrono_literals;

struct resource {
    void access() { /*...*/
    }
    bool is_ready() { return true; }
};

class producer_thread;
using producer_thread_base = satos::thread<producer_thread>;

class producer_thread : public producer_thread_base {
    friend producer_thread_base;

public:
    resource shared_resource;
    satos::mutex mtx;
    satos::condition_variable cv;

private:
    [[noreturn]] void thread_function() {
        while (true) {
            mtx.lock();
            shared_resource.access();
            mtx.unlock();
            cv.notify_one();
            satos::this_thread::sleep_for(100ms);
        }
    }
};

class consumer_thread;
using consumer_thread_base = satos::thread<consumer_thread>;

class consumer_thread : public consumer_thread_base {
    friend consumer_thread_base;

public:
    explicit consumer_thread(producer_thread& producer) : _producer(producer) {}

private:
    [[noreturn]] void thread_function() {
        while (true) {
            std::unique_lock lck(_producer.mtx);
            _producer.cv.wait(lck);
            _producer.shared_resource.access();
        }
    }

    producer_thread& _producer;
};

class consumer_thread_extended;
using consumer_thread_extended_base = satos::thread<consumer_thread_extended>;

class consumer_thread_extended : public consumer_thread_extended_base {
    friend consumer_thread_extended_base;

public:
    explicit consumer_thread_extended(producer_thread& producer) : _producer(producer) {}

private:
    [[noreturn]] void thread_function() {
        while (true) {
            std::unique_lock lck(_producer.mtx);
            if (_producer.cv.wait_for(
                    lck, 500ms, [this]() { return _producer.shared_resource.is_ready(); })) {
                _producer.shared_resource.access();
            } else {
                // blame producer
            }
        }
    }

    producer_thread& _producer;
};

int main() {
    static producer_thread producer;
    static consumer_thread consumer{producer};
    static consumer_thread_extended consumer_extended{producer};

    satos::kernel::initialize(100_MHz, 100_Hz);

    producer.start();
    consumer.start();
    consumer_extended.start();

    satos::kernel::start();
    while (true) {}
}
