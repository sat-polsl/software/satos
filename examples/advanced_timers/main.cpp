#include "satext/units.h"
#include "satos/kernel.h"
#include "satos/thread.h"
#include "satos/timer.h"

using namespace std::chrono_literals;

struct gpio {
    void high() { /*...*/
    }
    void low() { /*...*/
    }
};

class output_thread;
using output_thread_base = satos::thread<output_thread>;

class output_thread : public output_thread_base {
    friend output_thread_base;

private:
    [[noreturn]] void thread_function() {
        _output_timer.register_callback([this]() { _output.low(); });

        while (true) {
            _output.high();
            _output_timer.start();
            satos::this_thread::sleep_for(1s);
        }
    }

    gpio _output;
    satos::timer _output_timer{100ms, false};
};

int main() {
    static output_thread thread;

    satos::kernel::initialize(100_MHz, 100_Hz);
    thread.start();
    satos::kernel::start();
    while (true) {}
}
