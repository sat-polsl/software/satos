#include "satext/units.h"
#include "satos/kernel.h"
#include "satos/semaphore.h"
#include "satos/thread.h"

using namespace std::chrono_literals;

struct resource {
    void write(){/*...*/};
    void read(){/*...*/};
};

class producer_thread;
using producer_thread_base = satos::thread<producer_thread>;

class producer_thread : public producer_thread_base {
    friend producer_thread_base;

public:
    explicit producer_thread(resource& res, satos::binary_semaphore& sem) :
        _shared_resource{res},
        _semaphore{sem} {}

private:
    [[noreturn]] void thread_function() {
        while (true) {
            _shared_resource.write();
            _semaphore.release();
            satos::this_thread::sleep_for(100ms);
        }
    }

    resource& _shared_resource;
    satos::binary_semaphore& _semaphore;
};

class consumer_thread;
using consumer_thread_base = satos::thread<consumer_thread>;

class consumer_thread : public consumer_thread_base {
    friend consumer_thread_base;

public:
    explicit consumer_thread(resource& res, satos::binary_semaphore& sem) :
        _shared_resource{res},
        _semaphore{sem} {}

private:
    [[noreturn]] void thread_function() {
        while (true) {
            _semaphore.acquire();
            _shared_resource.read();
        }
    }

    resource& _shared_resource;
    satos::binary_semaphore& _semaphore;
};

int main() {
    static satos::binary_semaphore sem;
    static resource shared_resource;
    static producer_thread producer{shared_resource, sem};
    static consumer_thread consumer{shared_resource, sem};

    atos::kernel::initialize(100_MHz, 100_Hz);

    producer.start();
    consumer.start();

    satos::kernel::start();
    while (true) {}
}
