#include "satext/units.h"
#include "satos/kernel.hpp"
#include "satos/thread.hpp"

using namespace std::chrono_literals;

class example_thread;
using example_thread_base = satos::thread<example_thread>;

class example_thread : public example_thread_base {
    friend example_thread_base;

private:
    [[noreturn]] void thread_function() {
        while (true) {
            do_stuff();
            satos::this_thread::sleep_for(200ms);
        }
    }

    void do_stuff() { /*...*/
    }
};

class example_thread_extended;
using example_thread_extended_base =
    satos::thread<example_thread_extended, satos::thread_priority::above_normal_0, 1024, "thread">;

class example_thread_extended : public example_thread_extended_base {
    friend example_thread_extended_base private : [[noreturn]] void thread_function() {
        while (true) {
            do_time_consuming_stuff();
            satos::this_thread::sleep_until(next_time_point(satos::kernel::clock::now(), 100ms));
        }
    }

    void do_time_consuming_stuff() { /*...*/
    }

    static satos::kernel::clock::time_point
    next_time_point(const satos::kernel::clock::time_point& now,
                    const satos::kernel::clock::duration& interval) {
        return now + interval - (now.time_since_epoch() % interval);
    }
};

int main() {
    static example_thread ex_thread;
    static example_thread_extended ex_thread_ext;

    satos::kernel::initialize(100_MHz, 100_Hz);

    ex_thread.start();
    ex_thread_ext.start();

    satos::kernel::start();
    while (true) {}
}
