# Hello!
Nice to have you here! Feel free to contribute, submit issues and ask questions!

# Table of contents

- [Reporitng an Issue](#reporting-an-issue)
- [Working with repsitory](#working-with-repository)
    - [Contribution flow](#contribution-flow)
    - [Commit message format](#commit-message-format)
- [Coding style](#coding-style)
- [Useful links](#useful-links)

## Reporting an issue 
Bugs are tracked with [GitLab Issue Board](https://gitlab.com/sat-polsl/software/satos/-/boards)

Explain the problem and include additional details to help maintainers reproduce the problem.

## Working with repository
### Contribution flow

1. Branch out from `master` branch and name your branch `satos<issue_number>-<shortdescription>`.
2. Make commits to your branch.
3. Make sure to pass all tests and add new if needed.
4. Push your work to remote branch
5. Submit a Merge Request to `master` branch

### Commit message format
Use [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/)

## Coding style
Use `snake_case` for everything except template arguments which are named with `CamelCase`

## Useful links

- [Project wiki](https://sat-polsl.gitlab.io/wiki/satos/#/)

