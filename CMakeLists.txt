cmake_minimum_required(VERSION 3.25)
set(CMAKE_CONFIGURATION_TYPES Debug Release)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

option(SATOS_TEST "" OFF)
option(ENABLE_COVERAGE "Enable coverage reporting" OFF)

if (CMAKE_CURRENT_SOURCE_DIR STREQUAL CMAKE_SOURCE_DIR)
    include(${CMAKE_SOURCE_DIR}/cmake/toolchain.cmake)
    set(SATOS_TEST ON CACHE BOOL "" FORCE)
endif()

include(cmake/cpm.cmake)
include(cmake/packages.cmake)

project(satos)

set(MCU_CORES "cortex-m0" "cortex-m3" "cortex-m4" "cortex-m7")
set(TARGET_CORE "" CACHE STRING "Target MCU architecture of the project")
if(NOT TARGET_CORE IN_LIST MCU_CORES)
    message(FATAL_ERROR "Specified MCU core is not supported.\n"
        "To properly run this project use cmake -D TARGET_CORE=<core>.\n"
        "Currently these ones are supported: ${MCU_CORES}")
endif()

option(SATOS_DYNAMIC_ALLOCATION "Enable using dynamic allocation by satos" OFF)
set(SATOS_HEAP_SIZE 0 CACHE STRING "Heap size")
set(SATOS_NVIC_PRIORITY_BITS 4 CACHE STRING "Number of bits used for NVIC priority")
set(SATOS_MAX_SYSCALL_INTERRUPT_PRIORITY 5 CACHE STRING "Maximum syscall interrupt priority")
set(SATOS_PORT "" CACHE STRING "Port for satos")

add_subdirectory(satos)
add_subdirectory(satos_formatters)
add_subdirectory(satos_newlib)
add_subdirectory(satos_mock)
add_subdirectory(libs)

if(SATOS_TEST)
    include(GoogleTest)
    include(CTest)
    enable_testing()

    add_subdirectory(test)
endif()
